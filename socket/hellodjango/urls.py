from django.conf import settings
from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.views.generic.simple import redirect_to

admin.autodiscover()

urlpatterns = patterns('',
    ("^chat/", include('chat.urls')),
    url("", include('django_socketio.urls')),
    url(r'^admin/', include(admin.site.urls)),
    (r'^$', redirect_to, {'url': '/chat'})
)

if settings.DEBUG:
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
    )