Django==1.3.1
django-redis==1.4.5
gunicorn
git+https://github.com/johncosta/django-socketio.git
